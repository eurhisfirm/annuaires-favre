import { db } from "../database"
import {
  validateStringToNumber,
  validateChain,
  validateInteger,
  validateString,
  validateTest,
} from "../validators/core"

export async function get(req, res) {
  const [query, error] = validateQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in pages query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid pages query",
          },
        },
        null,
        2,
      ),
    )
  }

  const pages = (await db.any(
    `
      SELECT
        distinct page
      FROM lines
      where year = $<year>
      ORDER BY page
    `,
    query,
  )).map(({ page }) => page)

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(pages, null, 2))
}

function validateQuery(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  {
    const key = "year"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateString,
      validateStringToNumber,
      validateInteger,
      validateTest(
        value => value >= 1700 && value < 2000,
        "Expected a year between 1700 and 1999",
      ),
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

import { db } from "../database"
import { slugify } from "../strings"
import { validateNonEmptyTrimmedString } from "../validators/core"

export async function post(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const [body, error] = await validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in form:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Form contains errors",
          },
        },
        null,
        2,
      ),
    )
  }

  const result = { ...body }
  const { id } = await db.one(
    `
      INSERT INTO jobs (
        name,
        slug
      )
      VALUES (
        $<jobName>,
        $<slug>
      )
      RETURNING id
    `,
    {
      jobName: body.jobName,
      slug: slugify(body.jobName),
    },
  )
  result.id = id

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(result, null, 2))
}

async function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Value is empty."]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got "${typeof data}".`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  for (let key of ["jobName"]) {
    remainingKeys.delete(key)
    let [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error === null) {
      const slug = slugify(value)
      if (!slug) {
        error = "Le texte ne contient aucun caractère signifiant."
      } else if (
        (await db.one("SELECT EXISTS(SELECT * FROM jobs WHERE slug=$1)", [
          slug,
        ])).exists
      ) {
        error = "Un métier ayant un nom similaire existe déjà."
      }
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

import { db } from "../database"
import {
  validateBoolean,
  validateChain,
  validateInteger,
  validateMaybeTrimmedString,
  validateMissing,
  validateNonEmptyTrimmedString,
  validateOption,
  validateTest,
} from "../validators/core"

export async function post(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in form:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Form contains errors",
          },
        },
        null,
        2,
      ),
    )
  }

  const result = { ...body }
  if (body.lineId === null) {
    const { id } = await db.one(
      `
        INSERT INTO lines (
          city_id,
          city_name,
          comment,
          corporation_id,
          corporation_name,
          deleted,
          district_id,
          district_name,
          page,
          temporary,
          fair,
          job_id,
          job_name,
          user_id,
          year,
          created_at,
          updated_at
        )
        VALUES (
          $<cityId>,
          $<cityName>,
          $<comment>,
          $<corporationId>,
          $<corporationName>,
          $<deleted>,
          $<districtId>,
          $<districtName>,
          $<page>,
          $<temporary>,
          $<fair>,
          $<jobId>,
          $<jobName>,
          $<userId>,
          $<year>,
          current_timestamp,
          current_timestamp
        )
        RETURNING id
      `,
      {
        ...body,
        userId: user.id,
      },
    )
    result.lineId = id
  } else {
    await db.none(
      `
        UPDATE lines
        SET
          city_id = $<cityId>,
          city_name = $<cityName>,
          comment = $<comment>,
          corporation_id = $<corporationId>,
          corporation_name = $<corporationName>,
          deleted = $<deleted>,
          district_id = $<districtId>,
          district_name = $<districtName>,
          page = $<page>,
          temporary = $<temporary>,
          fair = $<fair>,
          job_id = $<jobId>,
          job_name = $<jobName>,
          user_id = $<userId>,
          year = $<year>,
          updated_at = current_timestamp
        WHERE id = $<lineId>
      `,
      {
        ...body,
        userId: user.id,
      },
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(result, null, 2))
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Value is empty."]
  }
  if (typeof data !== "object") {
    return [
      data,
      `Expected an object, got "${typeof data}".`,
    ]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  for (let key of ["cityId", "corporationId", "districtId", "page", "year"]) {
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateInteger,
      validateTest(value => value >= 0, "Number must be greater than or equal to zero."),
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["cityName", "corporationName", "districtName"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["comment", "jobName"]) {
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["deleted", "fair", "temporary"]) {
    remainingKeys.delete(key)
    const [value, error] = validateBoolean(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["jobId"]) {
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateInteger,
        validateTest(
          value => value >= 0,
          "Number must be greater than or equal to zero.",
        ),
      ],
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }
  if (errors.jobId === undefined && errors.jobName === undefined) {
    if (data.jobId === null && data.jobName !== null) {
      errors.jobId = "Missing value"
    } else if (data.jobId !== null && data.jobName === null) {
      errors.jobId = "Unexpected value: should be empty because name is missing"
    }
  }

  {
    const key = "lineId"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateInteger,
        validateTest(
          value => value >= 0,
          "Number must be greater than or equal to zero.",
        ),
      ],
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

import { CachedSyncIterable } from "cached-iterable"
import { FluentBundle } from "fluent"
import { mapBundleSync } from "fluent-sequence"
import { negotiateLanguages } from "fluent-langneg"

export const acceptedLanguages = ["en", "en-US", "fr", "fr-FR"]
const localizedLanguages = ["en-US", "fr-FR"]

const bundleByLanguage = {}
const messagesByLanguage = {
  "en-US": `
About = About
Account_Creation = Account Creation
Add = Add
Authentication = Authentication
Authentication_needed = Authentication_needed
Bank = Bank
Begin_edition = Begin edition
City = City
Comment = Comment
Create = Create
Create_account = Create a new account
Delete = Delete
DFIH_Logo = DFIH Logo
District = District
District_needed = A district is needed to be able to choose a city.
Edit_page_subtitle = Edition of the lines in a page
Fair_Days = Fair Days
Favre_Yearbooks = Favre Yearbooks
Home = Home
Home_page_subtitle = Online interface to edit the text of Favre yearbooks
Invalid_ID = Invalid ID
Job = Job
Loading = Loading…
Mission_statement = Edition of Favre yearbooks
New_page = New page
New_year = New year
No_year_yet = No year has been entered yet.
Opening = Opening
Page = Page
Page_Edition = Page Edition
Password = Password
Report_issue = Report an issue
Search = Search
Sign_in = Sign in
Sign_out = Sign out
Sign_up = Sign up
Temporary = Temporary
Temporary_Office = Temporary Office
Undelete = Undelete
Update = Update
Username = Username
Year = Year
Years = Years
You_need_to_sign_in = You need to sign in to be able to edit.
  `,
  "fr-FR": `
About = À propos
Authentication = Authentification
Authentication_needed = Authentification nécessaire
Account_Creation = Création d'un compte
Add = Ajouter
Bank = Banque
Begin_edition = Commencer la saisie
City = Localité
Comment = Commentaire
Create = Créer
Create_account = Création d'un nouveau compte
Delete = Supprimer
DFIH_Logo = Logo de DFIH
District = Département
District_needed = Un département est nécessaire au choix d'une localité.
Edit_page_subtitle = Saisie des lignes d'une page
Fair_Days = Jours de foire
Favre_Yearbooks = Annuaires Favre
Home = Accueil
Home_page_subtitle = Interface en ligne pour saisir le texte des annuaires Favre
Invalid_ID = Identifiant incorrect
Job = Métier
Loading = Chargement en cours…
Mission_statement = Saisie des annuaires Favre
New_page = Nouvelle page
New_year = Nouvelle année
No_year_yet = Aucune année n'a encore été saisie.
Opening = Ouverture
Page = Page
Page_Edition = Saisie d'une page
Password = Mot de passe
Report_issue = Signaler un problème
Search = Rechercher
Sign_in = Connexion
Sign_out = Déconnexion
Sign_up = Création
Temporary = Temporaire
Temporary_Office = Guichet temporaire
Undelete = Annuler la suppression
Update = Modifier
Username = Nom d'utilisateur
Year = Année
Years = Années
You_need_to_sign_in = Vous devez vous connecter pour pouvoir saisir.
  `,
}

/*
 * `Localization` handles translation formatting and fallback.
 *
 * The current negotiated fallback chain of languages is stored in the
 * `Localization` instance in form of an iterable of `FluentBundle`
 * instances.  This iterable is used to find the best existing translation for
 * a given identifier.
 *
 * Code taken from: https://github.com/projectfluent/fluent.js/blob/master/fluent-react/src/localization.js
 */
export class Localization {
  constructor(bundles) {
    this.bundles = CachedSyncIterable.from(bundles)
  }

  getBundle(id) {
    return mapBundleSync(this.bundles, id)
  }

  /// Find a translation by `id` and format it to a string using `args`.
  getString(id, args, fallback) {
    const bundle = this.getBundle(id)

    if (bundle === null) {
      return fallback || id
    }

    const msg = bundle.getMessage(id)
    return bundle.format(msg, args)
  }
}

export function* generateBundles(userLanguages) {
  // Choose locales that are best for the user.
  const currentLanguages = negotiateLanguages(
    userLanguages,
    localizedLanguages,
    {
      defaultLocale: "en",
    },
  )

  for (const language of currentLanguages) {
    yield bundleByLanguage[language]
  }
}

for (const language of localizedLanguages) {
  const bundle = new FluentBundle(language)
  const errors = bundle.addMessages(messagesByLanguage[language])
  if (errors.length > 0) {
    console.warning(
      `Errors occurred when adding messages for "${language}" language:\n${JSON.stringify(
        errors,
        null,
        2,
      )}`,
    )
  }
  bundleByLanguage[language] = bundle
}
